package homework8;

public class TestModelCallService {

    public static void main(String[] args) {
        String name = "Ольга";
        String pre = "Врач";
        TestModel testModel = new TestModel();

        System.out.println("Девушка 1: " +  testModel.getConstName());

        System.out.println("Девушка 2: " + testModel.setName(name));

        System.out.println("Девушка 3: " + testModel.newName());

        TestModel testModel2 = new TestModel();

        System.out.println("Девушка 4:" + testModel2.setName("Ирина"));

        testModel.displayInfo(pre);

        if (testModel.equals(testModel2)){
            System.out.println("Они равны");
        }
    }
}


