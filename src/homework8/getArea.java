package homework8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class getArea {
    public static void main(String[] args) throws NumberFormatException, IOException {
        Rectangle rectangle = new Rectangle();
        Circle circle = new Circle();
        //Обьявляем переменные которые использовать будем
        double x = 0;
        double y = 0;
        double r = 0;
        double pi = Math.PI;  //Математическое значение пи

        //Строковые переменные для вывода запроса на выбор операции которую хотим сделать

        String c1 = "Если вы хотите вычислить площадь ";
        String c2 = "прямоугольника";
        String c3 = "круга";
        String ch = "введите";
        String c4 = "Если вы хотите вычислить периметр";
        //вводим переменную для определения операции которую будем делать

        int l = 0;
        //Выводим запрос на выбор операции которую хотим сделать

        System.out.printf("%s  %s, %s 1\n\n", c1, c2, ch);
        System.out.printf("%s  %s, %s 2\n\n", c1, c3, ch);
        System.out.printf("%s  %s, %s 3\n\n", c4, c2, ch);
        System.out.printf("%s  %s, %s 4\n\n", c4, c3, ch);
        BufferedReader br = new BufferedReader //в переменную br записываем число что ввели с консоли
                (new InputStreamReader (System.in));

        try {
            l = Integer.parseInt(br.readLine());// контроль ввода корректных данных (если ввести символ , то выведет просьбу не путать программу)
        } catch (NumberFormatException e) {
            System.out.println(" Не путайте программу! ");
            System.exit(0);
        }
        if ((l > 4) || (l < 1)) { //проверка на ввод числа в пределах вариантов для исполнения
            System.out.println("Не путайте программу!");
            System.exit(0);
        }

        if (l == 1) {  //если ввели 1 выполняется этот блок
            //Ввод данных
            BufferedReader br1 = new BufferedReader
                    (new InputStreamReader(System.in));//ввод br1 для переменной первой смежной стороны
            System.out.println("Введите длинну двух смежных сторон прямоугольника \n\n");
            x = Double.parseDouble(br1.readLine()); // запись значения x в br1

            BufferedReader br2 = new BufferedReader
                    (new InputStreamReader(System.in));//ввод br2 для переменной второй смежной стороны
            y = Double.parseDouble(br2.readLine());// запись значения y в br2
            //Печать результата с вызовом функции
            System.out.printf("Ответ =  %f\n", rectangle.rectangle(x, y));
        }
        if (l == 2) {  //если ввели 2 выполняется этот блок
            //Ввод данных
            BufferedReader br3 = new BufferedReader
                    (new InputStreamReader(System.in));   //ввод br3 для переменной радиуса круга
            System.out.println("Введите длинну радиуса круга \n\n");
            r = Double.parseDouble(br3.readLine());
            double r1 = r; //ввод переменной r1 равной r
            //Печать результата с вызовом функции
            System.out.printf("Ответ = %f\n", circle.circle(pi, r, r1));
        }
        if (l == 3){   //если ввели 3 выполняется этот блок
            //Ввод данных
            BufferedReader br4 = new BufferedReader
                    (new InputStreamReader(System.in));
            System.out.println("Введите длинну двух смежных сторон прямоугольника \n\n");
            x = Double.parseDouble(br4.readLine());

            BufferedReader br5 = new BufferedReader
                    (new InputStreamReader(System.in));
            y = Double.parseDouble(br5.readLine());
            //Печать результата с вызовом функции
            System.out.printf("Ответ =  %f\n", rectangle.perimetrrecyangle(x, y));
        }
        if (l==4){   //если ввели 4 выполняется этот блок
            //Ввод данных
            BufferedReader br6 = new BufferedReader
                    (new InputStreamReader(System.in));
            System.out.println("Введите длинну радиуса круга \n\n");
            r = Double.parseDouble(br6.readLine());
            //Печать результата с вызовом функции
            System.out.printf("Ответ =  %f\n", circle.circumference(pi, r));
        }
    }
}