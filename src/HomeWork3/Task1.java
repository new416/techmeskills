package HomeWork3;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        sortMax();
        sortMin();
    }

    private static void sortMax() {
        int[] arr = new int[]{11, 12, 5, 4, 8, 21};


        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                //Сравниваем два элемента
                if (arr[j] < arr[j + 1]) {
                    //Перестановка
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        for (int a : arr) {
            System.out.print(a + " ");
        }
        System.out.println();
    }
    private static void sortMin() {
        int[] arr = new int[]{3, 2, 1, 4, 5, 7};


        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                //Сравниваем два элемента
                if (arr[j] > arr[j + 1]) {
                    //Перестановка
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }

        }

        for (int a : arr) {
            System.out.print(a + " ");
        }
    }
}
