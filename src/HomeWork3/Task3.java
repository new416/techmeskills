package HomeWork3;

public class Task3 {

    public static void main(String[] args) {
        minDoubleMass();
    }

    private static void minDoubleMass() {
        int[][] arr = new int[][]{{11, 12, 5, 4, 1, 21}, {7, 9, 22, 45, 76, 94}};
        int minAll = arr[0][0];

        for (int i = 0; i < arr.length; i++) {
            int min = minOfColl(arr[i]);
            if (min < minAll) {
                minAll = min;
            }
        }
        System.out.println("Общий минимум " + minAll);
    }

    private static int minOfColl(int[] arrKup) {
        int minPart= arrKup[0];

        for (int i = 1; i < arrKup.length; i++) {
            if (arrKup[i] < minPart)
                minPart = arrKup[i];
        }
        System.out.println("Значение минимум " + minPart);
        return minPart;
    }
}
