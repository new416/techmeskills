package HomeWork3;

public class Task2 {

    public static void main(String[] args) {
        stupidSorting();
    }

    private static void stupidSorting() {
        int[] arr = new int[]{11, 12, 5, 4, 8, 21};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                //Сравниваем два элемента
                if (arr[j] > arr[j + 1]) {
                    //Перестановка
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    j = 0;
                } else {
                    j++;
                }
            }
        } for(int a : arr) {
            System.out.print(a + " ");
        }
    }
}
