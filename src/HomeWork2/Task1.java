package HomeWork2;

public class Task1 {

    public static void main(String[] args) {
        task1();
    }

    private static void task1() {
        int[] array = new int[]{5, -8, 1};
        int neg = 0;
        int posit = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                neg++;
            } else if (array[i] > 0) {
                posit++;
            }
        }
        System.out.println("Количество положительных чисел " + posit);
    }
}
