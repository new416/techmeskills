package HomeWork2;
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        task2();
    }

    private static void task2() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число ");
        int a = 0;
        int b = 0;
        int c = 0;
        if (sc.hasNext()) {
            a = sc.nextInt();
            b = sc.nextInt();
            c = sc.nextInt();
            sc.close();
        }
        int neg = 0;
        int posit = 0;
        if (a > 0) posit = posit + 1;
        else neg = neg + 1;

        if (b > 0) posit = posit + 1;
        else neg = neg + 1;

        if (c > 0) posit = posit + 1;
        else neg = neg + 1;
        System.out.println("количество отрицательных чисел: " + neg);
        System.out.println("количество положительных чисел: " + posit);

    }
}
