package HomeWork2;

public class Task3 {

    public static void main(String[] args) {
        task3();
    }

    private static void task3() {
        int[] mass = new int[]{1, 9, 4, -2, 3, 15, -11};
        int minimum = mass[0] + 1;
        for (int i = 0; i < mass.length; i++) {
            if (mass[i] < minimum) {
                minimum = mass[i];
            }
        }
        System.out.println(minimum);

    }
}
