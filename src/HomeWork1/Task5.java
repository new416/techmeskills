package HomeWork1;

public class Task5 {

    public static void main(String[] args) {
        TaskOne();
        TaskTwo();
        TaskThree();
        TaskFour();
        TaskFive();
    }

    private static void TaskOne() {
        int a = 23;
        int b = 45;

        if (a < b) {
            System.out.println(a);
        } else {
            System.out.println(b);
        }
    }
    private static void TaskTwo(){
        int a = 23;
        double b = 41;
        int c = a + (int) b ;

        System.out.println(c);
    }
    private static void TaskThree(){
        int s = 21;
        float x = 4.3f;
        int c = 12;
        float r = s * x * c;

        System.out.print((float) r );
    }
    private static void TaskFour(){
        int q = 10;
        float w = 5.7f;
        double e = (q/w);
        int r = (int) (e*1000 - Math.floor(e)*1000);

        System.out.println(r);
    }
    private static void TaskFive(){
        double a = 6.8 ;
        double b = 7.3 ;
        double summ = a + b ;
        int v = (int) summ;

        System.out.println(v);
    }
}
