package HomeWork7;

public enum Months {
    JANUARY(1,"Январь"), // Объекты
    FEBRUARY(2, "Февраль"),
    MARCH(3, "Март"),
    APRIL(4,"Апрель"),
    MAY(5,"Май"),
    JUNE(6,"Июнь"),
    JULY(7,"Июль"),
    AUGUST(8,"Август"),
    SEPTEMBER(9,"Сентябрь"),
    OCTOBER(10,"Октябрь"),
    NOVEMBER(11,"Ноябрь"),
    DECEMBER(12,"Декабрь");

    int numberOfMonths;
    String nameOfMonths;


    Months(int inputnumberOfMonths, String inputnameOfMonths){
        numberOfMonths = inputnumberOfMonths;
        nameOfMonths = inputnameOfMonths;
    }

    public static String getValueOfMonthNumber(int parametrFromScanner){
        Months[] dayWeeks = Months.values();
        for(Months dayWeek : dayWeeks){
            if(dayWeek.numberOfMonths == parametrFromScanner){
                return dayWeek.nameOfMonths;
            }
        }
        return null;
    }

}


