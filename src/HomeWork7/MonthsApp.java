package HomeWork7;

import java.util.Scanner;

public class MonthsApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int monthParam = scanner.nextInt();
        System.out.println(Months.getValueOfMonthNumber(monthParam));
    }
}
