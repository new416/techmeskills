package HomeWork7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class getArea {
    public static void main(String[] args) throws NumberFormatException, IOException {
        Rectangle rectangle = new Rectangle();
        Circle circle = new Circle();

        double x = 0;
        double y = 0;
        double r = 0;
        double pi = Math.PI;

        String c1 = "Если вы хотите вычислить площадь ";
        String c2 = "прямоугольника";
        String c3 = "круга";
        String ch = "введите";

        int l = 0;

        System.out.printf("%s  %s, %s 1\n\n", c1, c2, ch);
        System.out.printf("%s  %s, %s 2\n\n", c1, c3, ch);
        BufferedReader br = new BufferedReader
                (new InputStreamReader (System.in));

        try {
            l = Integer.parseInt(br.readLine());
        } catch (NumberFormatException e) {
            System.out.println(" Не путайте программу! ");
            System.exit(0);
        }
        if ((l > 2) || (l < 1)) {
            System.out.println("Не путайте программу!");
            System.exit(0);
        }

        if (l == 1) {
            //Ввод данных
            BufferedReader br1 = new BufferedReader

                    (new InputStreamReader(System.in));
            System.out.println("Введите длинн двух смежных сторон прямоугольника \n\n");
            x = Double.parseDouble(br1.readLine());

            BufferedReader br2 = new BufferedReader
                    (new InputStreamReader(System.in));
            y = Double.parseDouble(br2.readLine());
            //Печать результата с вызовом функции
            System.out.printf("Ответ =  %f\n", rectangle.rectangle(x, y));
        }
        if (l == 2) {
            //Ввод данных
            BufferedReader br3 = new BufferedReader
                    (new InputStreamReader(System.in));
            System.out.println("Введите длинну радиуса круга \n\n");
            r = Double.parseDouble(br3.readLine());
            double r1 = r;
            //Печать результата с вызовом функции
            System.out.printf("Ответ = %f\n", circle.circle(pi, r, r1));

        }
    }
}