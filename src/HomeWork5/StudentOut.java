package HomeWork5;

import java.util.Random;

public class StudentOut {

    public static void main(String[] args) {
        task();
    }

    private static void task() {

        String[] name = new String[]{"Sasha", "Pasha", "Dasha", "Jooly", "Natasha"};
        Student[] students = new Student[14];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student();
            student.setGrade(getRandom(10));
            student.setName(name[getRandom(4)]);
            student.setGroup(getRandom(4));
            students[i] = student;
        }
        System.out.println("Все студенты");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }

        System.out.println("Студенты третьей группы");
        for (int i = 0; i < students.length; i++) {
            students[i].getStudent3Group();
        }
    }

    private static int getRandom(int maxLimit) {
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}