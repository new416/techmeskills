package TestHomeWork;

public class User {
    public String name;
    public String login;

    User() {
    }
    User(String name,String login){
        this.name = name;
        this.login = login;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setLogin(String login){
        this.login = login;
    }

    @Override
    public String toString() {
        return "User{" + "name='" + name + '\'' +
                ", login =" + login  + '}';
        }
}
