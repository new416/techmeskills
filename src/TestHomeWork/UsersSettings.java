package TestHomeWork;

import java.util.Random;

public class UsersSettings {

    public static void main(String[] args) {
        UsersThings();
    }

    private static void UsersThings() {

        String[] name = new String[]{"Sasha", "Pasha", "Dasha", "Jooly", "Natasha"};
        String[] login = new String[]{"qwerty", "asdfgh", "zxcvbn", "poiuy", "lkjhgf"};
        User[] users = new User[15];

        for (int i = 0; i < users.length; i++) {
            User user = new User();
            user.setName(name[getRandom(5)]);
            user.setLogin(login[getRandom(5)]);
            users[i] = user;

        }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());

        }
    }
    private static int getRandom(int maxLimit){
        Random random = new Random();
        return random.nextInt(maxLimit);
    }
}
